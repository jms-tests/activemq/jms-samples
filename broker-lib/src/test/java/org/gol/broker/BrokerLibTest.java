package org.gol.broker;

import lombok.extern.slf4j.Slf4j;
import org.gol.broker.embedded.EmbeddedUnitBroker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.function.IntConsumer;
import java.util.function.Supplier;

import static java.util.Collections.synchronizedList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static java.util.stream.IntStream.rangeClosed;
import static org.awaitility.Awaitility.await;
import static org.awaitility.Duration.TEN_SECONDS;
import static org.gol.broker.helper.TaskHelper.runMultithreaded;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
class BrokerLibTest {

    private static final String BROKER_URL = "tcp://127.0.0.1:55555";
    private static final String CONNECTION_CLIENT_ID_PREFIX_REGEX = "saruman";
    private static final String TOPIC_NAME = "EmbeddedUnitBrokerTestTopic";
    private static final String QUEUE_NAME = "EmbeddedUnitBrokerTestQueue";

    private static final int MESSAGES_PER_PRODUCER = 10;
    private static final int NO_OF_PRODUCERS = 100;
    private static final int NO_OF_CONSUMERS = 4;
    private static final int NO_OF_QUEUE_MESSAGES = MESSAGES_PER_PRODUCER * NO_OF_PRODUCERS;
    private static final int NO_OF_TOPIC_MESSAGES = NO_OF_CONSUMERS * MESSAGES_PER_PRODUCER * NO_OF_PRODUCERS;

    private EmbeddedUnitBroker broker;
    private BrokerConnection brokerConnection;

    @BeforeEach
    void startBroker() throws Exception {
        broker = EmbeddedUnitBroker.of(BROKER_URL).start();
        brokerConnection = BrokerConnection.of(BROKER_URL).start();
    }

    @AfterEach
    void stopBroker() throws Exception {
        brokerConnection.stop();
        broker.stop();
    }

    @Test
    @DisplayName("tests message transport flow")
    void howToUseLib() {
        //given
        String queueMsg = "message to queue";
        String topicMsg = "message to topic";
        List<Message> receivedMessages = synchronizedList(new ArrayList<>());

        //when
        BrokerConnection connection = BrokerConnection.of(BROKER_URL).start();

        BrokerQueueConsumer.of(connection, QUEUE_NAME, receivedMessages::add);
        BrokerTopicConsumer.of(connection, TOPIC_NAME, receivedMessages::add);

        try (var queueProducer = BrokerQueueProducer.of(connection, QUEUE_NAME);
             var topicProducer = BrokerTopicProducer.of(connection, TOPIC_NAME)) {
            queueProducer.sendTextMessage(queueMsg);
            topicProducer.sendTextMessage(topicMsg);
        }

        await().atMost(TEN_SECONDS).until(() -> receivedMessages.size() == 2);
        connection.close();

        //then
        assertEquals(2, receivedMessages.size());
        assertThat(receivedMessages.stream()
                        .map(this::readTextMessage)
                        .map(Optional::get)
                        .collect(toList()),
                containsInAnyOrder(topicMsg, queueMsg));
    }

    @DisplayName("tests if each topic consumer received all messages from all producers running multithreaded")
    @Test()
    void topicTest() {
        //given
        List<String> result = synchronizedList(new ArrayList<>());
        Runnable producerTask = getProducerTask(() -> BrokerTopicProducer.of(brokerConnection, TOPIC_NAME));
        MessageListener listener = message -> readTextMessage(message).ifPresent(result::add);

        //when
        range(0, NO_OF_CONSUMERS).forEach(i -> BrokerTopicConsumer.of(brokerConnection, TOPIC_NAME, listener));
        runMultithreaded(4, NO_OF_PRODUCERS, producerTask);

        //then
        await().atMost(TEN_SECONDS).until(() -> result.size() == NO_OF_TOPIC_MESSAGES);
        assertEquals(NO_OF_TOPIC_MESSAGES, result.size());
    }

    @DisplayName("tests if queue consumers received all messages from all producers running multithreaded")
    @Test()
    void testQueue() {
        //given
        List<String> result = synchronizedList(new ArrayList<>());
        Runnable producerTask = getProducerTask(() -> BrokerQueueProducer.of(brokerConnection, QUEUE_NAME));
        MessageListener listener = message -> readTextMessage(message).ifPresent(result::add);

        //when
        List<Future> futures = runMultithreaded(4, NO_OF_PRODUCERS, producerTask);
        await().atMost(TEN_SECONDS).until(() -> futures.stream().allMatch(Future::isDone));
        range(0, NO_OF_CONSUMERS).forEach(i -> BrokerQueueConsumer.of(brokerConnection, QUEUE_NAME, listener));

        //then
        await().atMost(TEN_SECONDS).until(() -> result.size() == NO_OF_QUEUE_MESSAGES);
        assertEquals(NO_OF_QUEUE_MESSAGES, result.size());
    }

    @Test
    void oneConnectionTest() {
        BrokerConnection connection = BrokerConnection.of(BROKER_URL).start();
        log.info("Client connection ID: {}", connection.getClientID());
        assertThat(connection.getClientID(), startsWith(CONNECTION_CLIENT_ID_PREFIX_REGEX));

        connection.close();
        assertThrows(BrokerException.class, connection::getClientID);
    }

    @Test
    void multipleConnectionsTest() {
        //given
        List<BrokerConnection> connectionList = new ArrayList<>();
        IntConsumer addConnection = i -> {
            BrokerConnection connection = BrokerConnection.of(BROKER_URL);
            connectionList.add(connection);
            connection.start();
        };

        //when
        range(0, 10).forEach(addConnection);

        //then
        assertEquals(10, connectionList.size());
    }

    private <T extends BrokerProducer> Runnable getProducerTask(Supplier<T> brokerProducerSupplier) {
        return () -> {
            try (var producer = brokerProducerSupplier.get()) {
                rangeClosed(1, MESSAGES_PER_PRODUCER)
                        .mapToObj(i -> "message " + i + " from " + Thread.currentThread().getName())
                        .forEach(producer::sendTextMessage);
            }
        };
    }

    private Optional<String> readTextMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
                String text = ((TextMessage) message).getText();
                log.debug("message received: {}", text);
                return Optional.ofNullable(text);
            } catch (JMSException e) {
                log.error("unable to read message payload", e);
            }
        } else {
            log.error("received unsupported message type");
        }
        return Optional.empty();
    }

}
