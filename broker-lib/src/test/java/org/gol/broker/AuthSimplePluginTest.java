package org.gol.broker;

import org.gol.broker.embedded.EmbeddedSimpleAuthUnitBroker;
import org.gol.broker.embedded.EmbeddedUnitBroker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.jms.JMSSecurityException;
import javax.jms.Message;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.synchronizedList;
import static java.util.stream.IntStream.rangeClosed;
import static org.awaitility.Awaitility.await;
import static org.awaitility.Duration.TEN_SECONDS;
import static org.junit.jupiter.api.Assertions.*;

class AuthSimplePluginTest {

    private static final String TCP_TRANSPORT_URI = "tcp://localhost:55555";
    private static final String TOPIC_NAME = "EmbeddedUnitBrokerTestTopic";

    private EmbeddedUnitBroker broker;

    @BeforeEach
    void startBroker() throws Exception {
        broker = EmbeddedSimpleAuthUnitBroker.of(TCP_TRANSPORT_URI).start();
    }

    @AfterEach
    void stopBroker() throws Exception {
        broker.stop();
    }

    @Test
    @DisplayName("create connection - without credentials - should throw security exception")
    void createConnectionWithoutCredentials() {
        var e = assertThrows(BrokerException.class, () -> BrokerConnection.of(TCP_TRANSPORT_URI).start());
        assertTrue(e.getCause() instanceof JMSSecurityException);
    }

    @Test
    @DisplayName("create connection - with incorrect credentials - should throw security exception")
    void createConnectionWithIncorrectCredentials() {
        var e = assertThrows(BrokerException.class, () -> BrokerConnection.of(TCP_TRANSPORT_URI, "x", "y").start());
        assertTrue(e.getCause() instanceof JMSSecurityException);
    }

    @Test
    @DisplayName("create connection - with credentials - should connect")
    void createConnectionWithCredentials() {
        var connection = BrokerConnection.of(TCP_TRANSPORT_URI, "admin", "admin123").start();
        assertNotNull(connection.getClientID());
    }

    @Test
    @DisplayName("message transport - through allowed destinations - should transfer message")
    void messageTransportThroughAllowedDestination() {
        //given
        var publisherConnection = BrokerConnection.of(TCP_TRANSPORT_URI, "publisher", "publisher123").start();
        var consumerConnection = BrokerConnection.of(TCP_TRANSPORT_URI, "consumer", "consumer123").start();
        var topic = "SECURED.test";
        List<Message> receivedMessages = synchronizedList(new ArrayList<>());

        //when
        var topicProducer = BrokerTopicProducer.of(publisherConnection, topic);
        BrokerTopicConsumer.of(consumerConnection, topic, receivedMessages::add);
        rangeClosed(1, 10)
                .forEach(i -> topicProducer.sendTextMessage(String.format("topic message %d", i)));

        //then
        await().atMost(TEN_SECONDS).until(() -> receivedMessages.size() == 10);
        assertEquals(10, receivedMessages.size());
    }

    @Test
    @DisplayName("message transport - through allowed guest destinations - should transfer message")
    void messageTransportThroughAllowedGuestDestination() {
        //given
        var connection = BrokerConnection.of(TCP_TRANSPORT_URI, "publisher", "publisher123").start();
        var guestConnection = BrokerConnection.of(TCP_TRANSPORT_URI, "guest", "guest123").start();
        var topic = "SECURED.guest";
        List<Message> receivedMessages = synchronizedList(new ArrayList<>());

        //when
        var topicProducer = BrokerTopicProducer.of(connection, topic);
        BrokerTopicConsumer.of(guestConnection, topic, receivedMessages::add);
        rangeClosed(1, 10)
                .forEach(i -> topicProducer.sendTextMessage(String.format("topic message %d", i)));


        //then
        await().atMost(TEN_SECONDS).until(() -> receivedMessages.size() == 10);
        assertEquals(10, receivedMessages.size());
    }

    @Test
    @DisplayName("connect publisher - when destination is disallowed - should throw security exception")
    void connectPublisherToDisallowedDestination() {
        //given
        var connection = BrokerConnection.of(TCP_TRANSPORT_URI, "guest", "guest123").start();
        var topic = "SECURED.test";

        //when and then
        var e = assertThrows(BrokerException.class, () -> BrokerTopicProducer.of(connection, topic));
        assertEquals(JMSSecurityException.class, e.getCause().getClass());
    }

    @Test
    @DisplayName("connect consumer - when destination is disallowed - should throw security exception")
    void connectListenerToDisallowedDestination() {
        //given
        var connection = BrokerConnection.of(TCP_TRANSPORT_URI, "guest", "guest123").start();
        var topic = "SECURED.test";

        //when and then
        var e = assertThrows(BrokerException.class, () -> BrokerTopicConsumer.of(connection, topic, new BrokerTextListener()));
        assertEquals(JMSSecurityException.class, e.getCause().getClass());
    }

}
