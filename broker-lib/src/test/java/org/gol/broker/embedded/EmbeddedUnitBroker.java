package org.gol.broker.embedded;

import org.apache.activemq.broker.BrokerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EmbeddedUnitBroker {

    private static final String BROKER_NAME = "EmbeddedUnitBroker";

    BrokerService brokerService;

    EmbeddedUnitBroker() throws Exception {
        brokerService = new BrokerService();
        brokerService.setBrokerName(BROKER_NAME);
        brokerService.setPersistent(false);
    }

    EmbeddedUnitBroker(String connectorUrl) throws Exception {
        this();
        brokerService.addConnector(connectorUrl);
    }

    public static EmbeddedUnitBroker of(String connectorUrl) throws Exception {
        EmbeddedUnitBroker broker = new EmbeddedUnitBroker(connectorUrl);
        return broker;
    }

    public EmbeddedUnitBroker start() throws Exception {
        brokerService.start();
        log.debug("{} has started", BROKER_NAME);
        return this;
    }

    public void stop() throws Exception {
        brokerService.stop();
        log.debug("{} has stopped", BROKER_NAME);
    }

}
