package org.gol.broker.embedded;

import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.broker.SslContext;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import static javax.net.ssl.KeyManagerFactory.getDefaultAlgorithm;

@Slf4j
public class EmbeddedSslUnitBroker extends EmbeddedUnitBroker {

    private static final String KEY_STORE = "src/test/resources/ssl/amq-server.ks";
    private static final String KEY_STORE_PASSWORD = "server-keystore";
    private static final String TRUST_STORE = "src/test/resources/ssl/amq-server.ts";
    private static final String TRUST_STORE_PASSWORD = "server-truststore";

    private EmbeddedSslUnitBroker(String connectorUrl) throws Exception {
        super();
        brokerService.setSslContext(createSslContext());
        brokerService.addConnector(connectorUrl + "?maximumConnections=100");
    }

    public static EmbeddedSslUnitBroker of(String connectorUrl) throws Exception {
        EmbeddedSslUnitBroker broker = new EmbeddedSslUnitBroker(connectorUrl);
        return broker;
    }

    private SslContext createSslContext() {
        var sslContext = new SslContext();
        sslContext.addKeyManager(createKeyManager());
        sslContext.addTrustManager(createTrustManager());

        return sslContext;
    }

    private KeyManager createKeyManager() {
        try {
            var keyStore = KeyStore.getInstance(Paths.get(KEY_STORE).toFile(), KEY_STORE_PASSWORD.toCharArray());
            var keyStoreManagerFactory = KeyManagerFactory.getInstance(getDefaultAlgorithm());
            keyStoreManagerFactory.init(keyStore, KEY_STORE_PASSWORD.toCharArray());
            return keyStoreManagerFactory.getKeyManagers()[0];
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException | UnrecoverableKeyException e) {
            log.error("KeyManager creation error", e);
            throw  new IllegalStateException("TrustManager creation error", e);
        }
    }

    private TrustManager createTrustManager() {
        try {
            var trustStore = KeyStore.getInstance(Paths.get(TRUST_STORE).toFile(), TRUST_STORE_PASSWORD.toCharArray());
            var trustStoreManagerFactory = TrustManagerFactory.getInstance(getDefaultAlgorithm());
            trustStoreManagerFactory.init(trustStore);
            return trustStoreManagerFactory.getTrustManagers()[0];
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException e) {
            log.error("TrustManager creation error", e);
            throw  new IllegalStateException("TrustManager creation error", e);
        }
    }

}
