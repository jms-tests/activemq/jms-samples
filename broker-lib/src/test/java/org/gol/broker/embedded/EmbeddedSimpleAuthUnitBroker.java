package org.gol.broker.embedded;

import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.broker.BrokerPlugin;
import org.apache.activemq.security.*;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

@Slf4j
public class EmbeddedSimpleAuthUnitBroker extends EmbeddedUnitBroker {

    private EmbeddedSimpleAuthUnitBroker(String connectorUrl) throws Exception {
        super(connectorUrl);
        brokerService.setPlugins(new BrokerPlugin[]{prepareAuthentication(), prepareAuthorization()});
    }

    public static EmbeddedSimpleAuthUnitBroker of(String connectorUrl) throws Exception {
        EmbeddedSimpleAuthUnitBroker broker = new EmbeddedSimpleAuthUnitBroker(connectorUrl);
        return broker;
    }

    private SimpleAuthenticationPlugin prepareAuthentication() {
        var users = new ArrayList<AuthenticationUser>();
        users.add(new AuthenticationUser("admin", "admin123", "admins, publishers, consumers"));
        users.add(new AuthenticationUser("publisher", "publisher123", "publishers,consumers"));
        users.add(new AuthenticationUser("consumer", "consumer123", "consumers"));
        users.add(new AuthenticationUser("guest", "guest123", "guests"));

        return new SimpleAuthenticationPlugin(users);
    }

    private AuthorizationPlugin prepareAuthorization() throws Exception {
        var authorizationMap = new DefaultAuthorizationMap(List.of(
                prepareTopicAuthorization("SECURED.guest", "guests", null, null),
                prepareTopicAuthorization("SECURED.>", "consumers", "publishers", "publishers"),
                prepareTopicAuthorization(">", "admins", "admins", "admins"),
                prepareTopicAuthorization(
                        "ActiveMQ.Advisory.>",
                        "admins,publishers,consumers,guests",
                        "admins,publishers,consumers,guests",
                        "admins,publishers,consumers,guests")));
        return new AuthorizationPlugin(authorizationMap);
    }

    private static AuthorizationEntry prepareTopicAuthorization(String topicName, String readRoles, String writeRoles, String adminRoles) throws Exception {
        AuthorizationEntry auth = new AuthorizationEntry();
        auth.setTopic(topicName);

        if (nonNull(readRoles)) {
            auth.setRead(readRoles);
        }

        if (nonNull(writeRoles)) {
            auth.setWrite(writeRoles);
        }

        if (nonNull(adminRoles)) {
            auth.setAdmin(adminRoles);
        }

        return auth;
    }

}
