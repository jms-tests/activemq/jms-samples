package org.gol.broker.embedded;

import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.broker.BrokerPlugin;
import org.apache.activemq.security.*;

import java.util.List;

import static java.util.Objects.nonNull;

@Slf4j
public class EmbeddedJaasAuthUnitBroker extends EmbeddedUnitBroker {

    private static final String CONFIG_FILE = "src/test/resources/jaas/login.config";

    private EmbeddedJaasAuthUnitBroker(String connectorUrl) throws Exception {
        super(connectorUrl);
        System.setProperty("java.security.auth.login.config", CONFIG_FILE);
        brokerService.setPlugins(new BrokerPlugin[]{prepareAuthentication(), prepareAuthorization()});
    }

    public static EmbeddedJaasAuthUnitBroker of(String connectorUrl) throws Exception {
        EmbeddedJaasAuthUnitBroker broker = new EmbeddedJaasAuthUnitBroker(connectorUrl);
        return broker;
    }

    private JaasAuthenticationPlugin prepareAuthentication() {
        var jaasAuthenticationPlugin = new JaasAuthenticationPlugin();
        jaasAuthenticationPlugin.setConfiguration("activemq");
        return jaasAuthenticationPlugin;
    }

    private AuthorizationPlugin prepareAuthorization() throws Exception {
        var authorizationMap = new DefaultAuthorizationMap(List.of(
                prepareTopicAuthorization("SECURED.guest", "guests", null, null),
                prepareTopicAuthorization("SECURED.>", "consumers", "publishers", "publishers"),
                prepareTopicAuthorization(">", "admins", "admins", "admins"),
                prepareTopicAuthorization(
                        "ActiveMQ.Advisory.>",
                        "admins,publishers,consumers,guests",
                        "admins,publishers,consumers,guests",
                        "admins,publishers,consumers,guests")));
        return new AuthorizationPlugin(authorizationMap);
    }

    private static AuthorizationEntry prepareTopicAuthorization(String topicName, String readRoles, String writeRoles, String adminRoles) throws Exception {
        AuthorizationEntry auth = new AuthorizationEntry();
        auth.setTopic(topicName);

        if (nonNull(readRoles)) {
            auth.setRead(readRoles);
        }

        if (nonNull(writeRoles)) {
            auth.setWrite(writeRoles);
        }

        if (nonNull(adminRoles)) {
            auth.setAdmin(adminRoles);
        }

        return auth;
    }

}
