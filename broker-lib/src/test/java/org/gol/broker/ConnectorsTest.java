package org.gol.broker;

import org.apache.activemq.broker.BrokerService;
import org.gol.broker.embedded.EmbeddedSslUnitBroker;
import org.gol.broker.embedded.EmbeddedUnitBroker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

import static java.lang.System.setProperty;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
public class ConnectorsTest {

    private static final String SSL_TRANSPORT_URI = "ssl://localhost:55555";

    private static final String JAVAX_NET_SSL_KEY_STORE = "javax.net.ssl.keyStore";
    private static final String JAVAX_NET_SSL_KEY_STORE_PASSWORD = "javax.net.ssl.keyStorePassword";
    private static final String JAVAX_NET_SSL_TRUST_STORE = "javax.net.ssl.trustStore";
    private static final String JAVAX_NET_SSL_TRUST_STORE_PASSWORD = "javax.net.ssl.trustStorePassword";

    private static final String CLIENT_KEY_STORE = "src/test/resources/ssl/amq-client.ks";
    private static final String CLIENT_KEY_STORE_PASSWORD = "client-keystore";
    private static final String CLIENT_TRUST_STORE = "src/test/resources/ssl/amq-client.ts";
    private static final String CLIENT_TRUST_STORE_PASSWORD = "client-truststore";

    private BrokerService brokerService;
    private EmbeddedUnitBroker broker;

    @BeforeEach
    void startBroker() throws Exception {
        broker = EmbeddedSslUnitBroker.of(SSL_TRANSPORT_URI).start();
    }

    @AfterEach
    void stopBroker() throws Exception {
        broker.stop();
    }

    @Test
    @DisplayName("SSL connection - given Javax properties set correctly - should  connect")
    void sslConnectionWithJavaxPropertiesSet() {
        //given
        setProperty(JAVAX_NET_SSL_KEY_STORE, CLIENT_KEY_STORE);
        setProperty(JAVAX_NET_SSL_KEY_STORE_PASSWORD, CLIENT_KEY_STORE_PASSWORD);
        setProperty(JAVAX_NET_SSL_TRUST_STORE, CLIENT_TRUST_STORE);
        setProperty(JAVAX_NET_SSL_TRUST_STORE_PASSWORD, CLIENT_TRUST_STORE_PASSWORD);

        //when
        var connection = BrokerConnection.of(SSL_TRANSPORT_URI).start();

        //then
        assertNotNull(connection.getClientID());
        connection.close();
    }

    @Test
    @DisplayName("SSL connection - when correct key and trust store was given - should  connect")
    void sslConnectionWithSslFactory() throws Exception {
        //given
        //when
        var connection = BrokerConnection.ofSsl(
                SSL_TRANSPORT_URI,
                CLIENT_KEY_STORE,
                CLIENT_KEY_STORE_PASSWORD,
                CLIENT_TRUST_STORE,
                CLIENT_TRUST_STORE_PASSWORD).start();

        //then
        assertNotNull(connection.getClientID());
        connection.close();
    }

    @Test
    @DisplayName("SSL connection - when incorrect key and trust store password was given - should throw exception")
    void sslConnectionWithWrongCredentials() throws Exception {
        var brokerException = assertThrows(BrokerException.class, () -> BrokerConnection.ofSsl(
                SSL_TRANSPORT_URI,
                CLIENT_KEY_STORE,
                "wrong-password",
                CLIENT_TRUST_STORE,
                "wrong-password").start());
        log.error(brokerException.getCause().getMessage());
    }

}
