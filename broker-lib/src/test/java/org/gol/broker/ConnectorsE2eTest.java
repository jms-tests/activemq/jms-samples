package org.gol.broker;

import org.junit.jupiter.api.Test;

import static java.lang.System.setProperty;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ConnectorsE2eTest {

    private static final String TCP_TRANSPORT_URI = "tcp://localhost:61616";
    private static final String SSL_TRANSPORT_URI = "ssl://localhost:61617";

    private static final String CLIENT_KEY_STORE = "/opt/activemq/conf/amq-client.ks";
    private static final String CLIENT_KEY_STORE_PASSWORD = "client-keystore";
    private static final String CLIENT_TRUST_STORE = "/opt/activemq/conf/amq-client.ts";
    private static final String CLIENT_TRUST_STORE_PASSWORD = "client-truststore";

    private static final String JAVAX_NET_SSL_KEY_STORE = "javax.net.ssl.keyStore";
    private static final String JAVAX_NET_SSL_KEY_STORE_PASSWORD = "javax.net.ssl.keyStorePassword";
    private static final String JAVAX_NET_SSL_TRUST_STORE = "javax.net.ssl.trustStore";
    private static final String JAVAX_NET_SSL_TRUST_STORE_PASSWORD = "javax.net.ssl.trustStorePassword";

    @Test
    void tcpConnectionTest() {
        BrokerConnection connection = BrokerConnection.of(TCP_TRANSPORT_URI).start();
        assertNotNull(connection.getClientID());
        connection.close();
    }

    @Test
    void sslConnectionWithJavaxPropertiesSet() {
        setProperty(JAVAX_NET_SSL_KEY_STORE, CLIENT_KEY_STORE);
        setProperty(JAVAX_NET_SSL_KEY_STORE_PASSWORD, CLIENT_KEY_STORE_PASSWORD);
        setProperty(JAVAX_NET_SSL_TRUST_STORE, CLIENT_TRUST_STORE);
        setProperty(JAVAX_NET_SSL_TRUST_STORE_PASSWORD, CLIENT_TRUST_STORE_PASSWORD);

        BrokerConnection connection = BrokerConnection.of(SSL_TRANSPORT_URI).start();
        assertNotNull(connection.getClientID());
        connection.close();
    }

    @Test
    void sslConnectionWithSslFactory() throws Exception {
        BrokerConnection connection = BrokerConnection.ofSsl(
                SSL_TRANSPORT_URI,
                CLIENT_KEY_STORE,
                CLIENT_KEY_STORE_PASSWORD,
                CLIENT_TRUST_STORE,
                CLIENT_TRUST_STORE_PASSWORD).start();
        assertNotNull(connection.getClientID());
        connection.close();
    }

}
