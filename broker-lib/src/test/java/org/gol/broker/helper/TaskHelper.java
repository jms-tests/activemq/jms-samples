package org.gol.broker.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.stream.IntStream.range;

public class TaskHelper {

    public static List<Future> runMultithreaded(int threadsCount, int tasksCount, Runnable task) {
        List<Future> resultFutureList = new ArrayList<>();
        ExecutorService executorService = newFixedThreadPool(threadsCount);

        range(0, tasksCount)
                .mapToObj(i -> task)
                .forEach(t -> resultFutureList.add(executorService.submit(t)));

        executorService.shutdown();
        return resultFutureList;
    }

    public static <T> List<Future<T>> callMultithreaded(int threadsCount, int tasksCount, Callable<T> task) {
        List<Future<T>> resultFutureList = new ArrayList<>();
        ExecutorService executorService = newFixedThreadPool(threadsCount);

        range(0, tasksCount)
                .mapToObj(i -> task)
                .forEach(t -> resultFutureList.add(executorService.submit(t)));

        executorService.shutdown();
        return resultFutureList;
    }

}
