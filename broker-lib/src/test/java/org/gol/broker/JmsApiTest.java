package org.gol.broker;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import lombok.extern.slf4j.Slf4j;

import static java.util.Collections.synchronizedList;
import static java.util.stream.Collectors.toList;
import static javax.jms.Session.AUTO_ACKNOWLEDGE;
import static org.awaitility.Awaitility.await;
import static org.awaitility.Duration.TEN_SECONDS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
public class JmsApiTest {

    private static final String BROKER_NAME = "EmbeddedBroker";
    private static final String BROKER_URL = "tcp://127.0.0.1:55555";
    private static final String CONNECTION_CLIENT_ID_PREFIX_REGEX = "saruman";
    private static final String TOPIC_NAME = "EmbeddedUnitBrokerTestTopic";
    private static final String QUEUE_NAME = "EmbeddedUnitBrokerTestQueue";

    private static final int MESSAGES_PER_PRODUCER = 10;
    private static final int NO_OF_PRODUCERS = 100;
    private static final int NO_OF_CONSUMERS = 4;
    private static final int NO_OF_QUEUE_MESSAGES = MESSAGES_PER_PRODUCER * NO_OF_PRODUCERS;
    private static final int NO_OF_TOPIC_MESSAGES = NO_OF_CONSUMERS * MESSAGES_PER_PRODUCER * NO_OF_PRODUCERS;

    private BrokerService brokerService;

    @BeforeEach
    void startBroker() throws Exception {
        brokerService = new BrokerService();
        brokerService.setBrokerName(BROKER_NAME);
        brokerService.setPersistent(false);
        brokerService.addConnector(BROKER_URL);
        brokerService.start();
    }

    @AfterEach
    void stopBroker() throws Exception {
        brokerService.stop();
    }

    @Test
    @DisplayName("tests message transport flow")
    void howToUseJmsApi() throws JMSException {
        //given
        String queueMsg = "message to queue";
        String topicMsg = "message to topic";
        List<Message> receivedMessages = synchronizedList(new ArrayList<>());

        //when
        //create connection - one for many threads
        var connectionFactory = new ActiveMQConnectionFactory(BROKER_URL);
        connectionFactory.setClientIDPrefix(CONNECTION_CLIENT_ID_PREFIX_REGEX);
        var connection = connectionFactory.createConnection();
        connection.start();

        //create queue consumer
        var queueConsumerSession = connection.createSession(false, AUTO_ACKNOWLEDGE);
        var queueConsumerDestination = queueConsumerSession.createQueue(QUEUE_NAME);
        var queueConsumer = queueConsumerSession.createConsumer(queueConsumerDestination);
        queueConsumer.setMessageListener(receivedMessages::add);

        //create topic consumer
        var topicConsumerSession = connection.createSession(false, AUTO_ACKNOWLEDGE);
        var topicConsumerDestination = topicConsumerSession.createTopic(TOPIC_NAME);
        var topicConsumer = topicConsumerSession.createConsumer(topicConsumerDestination);
        topicConsumer.setMessageListener(receivedMessages::add);

        //create queue producer and send message
        var queueProducerSession = connection.createSession(false, AUTO_ACKNOWLEDGE);
        var queueProducerDestination = queueProducerSession.createQueue(QUEUE_NAME);
        try (var queueProducer = queueProducerSession.createProducer(queueConsumerDestination)) {
            var queueProducerMessage = queueProducerSession.createTextMessage(queueMsg);
            queueProducer.send(queueProducerMessage);
        }

        //create topic producer and send message
        var topicProducerSession = connection.createSession(false, AUTO_ACKNOWLEDGE);
        var topicProducerDestination = topicProducerSession.createQueue(TOPIC_NAME);
        try (var topicProducer = topicProducerSession.createProducer(queueConsumerDestination)) {
            var topicProducerMessage = topicProducerSession.createTextMessage(topicMsg);
            topicProducer.send(topicProducerMessage);
        }

        await().atMost(TEN_SECONDS).until(() -> receivedMessages.size() == 2);
        connection.close();

        //then
        assertEquals(2, receivedMessages.size());
        assertThat(receivedMessages.stream()
                        .map(this::readTextMessage)
                        .map(Optional::get)
                        .collect(toList()),
                containsInAnyOrder(topicMsg, queueMsg));
    }

    private Optional<String> readTextMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
                String text = ((TextMessage) message).getText();
                log.debug("message received: {}", text);
                return Optional.ofNullable(text);
            } catch (JMSException e) {
                log.error("unable to read message payload", e);
            }
        } else {
            log.error("received unsupported message type");
        }
        return Optional.empty();
    }
}
