package org.gol.broker;

import javax.jms.Destination;
import javax.jms.MessageListener;

public class BrokerQueueConsumer extends BrokerConsumer {

    private BrokerQueueConsumer(BrokerConnection brokerConnection, String destinationName, MessageListener messageListener) {
        super(brokerConnection, destinationName, messageListener);
    }

    public static BrokerQueueConsumer of(BrokerConnection brokerConnection, String destinationName, MessageListener messageListener) {
        return new BrokerQueueConsumer(brokerConnection, destinationName, messageListener);
    }

    @Override
    Destination createDestination(String name) {
        return getBrokerSession().createQueue(name);
    }
}
