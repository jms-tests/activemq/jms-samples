package org.gol.broker;

import javax.jms.Destination;

public class BrokerTopicProducer extends BrokerProducer {


    private BrokerTopicProducer(BrokerConnection brokerConnection, String destinationName) {
        super(brokerConnection, destinationName);
    }

    public static BrokerTopicProducer of(BrokerConnection brokerConnection, String destinationName) {
        return new BrokerTopicProducer(brokerConnection, destinationName);
    }

    @Override
    Destination createDestination(String name) {
        return getBrokerSession().createTopic(name);
    }
}
