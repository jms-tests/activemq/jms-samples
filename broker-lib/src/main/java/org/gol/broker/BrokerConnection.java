package org.gol.broker;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Session;

import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.ActiveMQSslConnectionFactory;

import static org.gol.broker.BrokerException.Message.*;
import static org.gol.broker.BrokerException.exceptionOf;

@Slf4j
public class BrokerConnection {

    private static final String CLIENT_ID_PREFIX = "saruman";

    private final Connection connection;

    private BrokerConnection(Connection connection) {
        this.connection = connection;
    }

    public static BrokerConnection of(String brokerUrl) {
        try {
            Connection connection = getConnectionFactory(brokerUrl).createConnection();
            return new BrokerConnection(connection);
        } catch (JMSException e) {
            throw exceptionOf(CANNOT_CREATE_CONNECTION, e);
        }
    }

    public static BrokerConnection of(String brokerUrl, String userName, String userPassword) {
        try {
            Connection connection = getConnectionFactory(brokerUrl).createConnection(userName, userPassword);
            return new BrokerConnection(connection);
        } catch (JMSException e) {
            throw exceptionOf(CANNOT_CREATE_CONNECTION, e);
        }
    }

    public static BrokerConnection ofSsl(
            String brokerUrl,
            String keyStore,
            String keyStorePassword,
            String trustStore,
            String trustStorePassword) {
        try {
            Connection connection = getSslConnectionFactory(brokerUrl, keyStore, keyStorePassword, trustStore, trustStorePassword).createConnection();
            return new BrokerConnection(connection);
        } catch (JMSException e) {
            throw exceptionOf(CANNOT_CREATE_CONNECTION, e);
        }
    }

    public static BrokerConnection ofSsl(
            String brokerUrl,
            String keyStore,
            String keyStorePassword,
            String trustStore,
            String trustStorePassword,
            String userName,
            String userPassword) {
        try {
            Connection connection = getSslConnectionFactory(brokerUrl, keyStore, keyStorePassword, trustStore, trustStorePassword)
                    .createConnection(userName, userPassword);
            return new BrokerConnection(connection);
        } catch (JMSException e) {
            throw exceptionOf(CANNOT_CREATE_CONNECTION, e);
        }
    }

    public BrokerConnection start() {
        try {
            connection.start();
            return this;
        } catch (JMSException e) {
            throw exceptionOf(CONNECTION_OPERATION_EXCEPTION, e);
        }
    }

    public void stop() {
        try {
            connection.stop();
        } catch (JMSException e) {
            throw exceptionOf(CONNECTION_OPERATION_EXCEPTION, e);
        }
    }

    public void close() {
        try {
            connection.close();
        } catch (JMSException e) {
            throw exceptionOf(CONNECTION_OPERATION_EXCEPTION, e);
        }
    }

    public String getClientID() {
        try {
            return connection.getClientID();
        } catch (JMSException e) {
            throw exceptionOf(CONNECTION_OPERATION_EXCEPTION, e);
        }
    }

    Session createSession() {
        try {
            return connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        } catch (JMSException e) {
            throw exceptionOf(CANNOT_CREATE_SESSION, e);
        }
    }

    private static ActiveMQConnectionFactory getConnectionFactory(String brokerUrl) {
        var connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
        connectionFactory.setClientIDPrefix(CLIENT_ID_PREFIX);
        return connectionFactory;
    }

    private static ActiveMQConnectionFactory getSslConnectionFactory(
            String brokerUrl,
            String keyStore,
            String keyStorePassword,
            String trustStore,
            String trustStorePassword) {
        var connectionFactory = new ActiveMQSslConnectionFactory(brokerUrl);
        connectionFactory.setClientIDPrefix(CLIENT_ID_PREFIX);

        try {
            connectionFactory.setKeyStore(keyStore);
            connectionFactory.setTrustStore(trustStore);
        } catch (Exception e) {
            throw exceptionOf(SSL_KEY_OR_TRUST_STORE_ERROR, e);
        }


        connectionFactory.setKeyStorePassword(keyStorePassword);
        connectionFactory.setTrustStorePassword(trustStorePassword);
        return connectionFactory;
    }

}
