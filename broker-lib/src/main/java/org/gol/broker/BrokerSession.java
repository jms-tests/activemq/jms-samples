package org.gol.broker;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

import static org.gol.broker.BrokerException.Message.SESSION_OPERATION_EXCEPTION;
import static org.gol.broker.BrokerException.exceptionOf;

class BrokerSession {

    private final Session session;

    private BrokerSession(Session session) {
        this.session = session;
    }

    static BrokerSession from(BrokerConnection brokerConnection) {
        return new BrokerSession(brokerConnection.createSession());
    }

    Queue createQueue(String name) {
        try {
            return session.createQueue(name);
        } catch (JMSException e) {
            throw exceptionOf(SESSION_OPERATION_EXCEPTION, e);
        }
    }

    Topic createTopic(String name) {
        try {
            return session.createTopic(name);
        } catch (JMSException e) {
            throw exceptionOf(SESSION_OPERATION_EXCEPTION, e);
        }
    }

    MessageProducer createProducer(Destination destination) {
        try {
            return session.createProducer(destination);
        } catch (JMSException e) {
            throw exceptionOf(SESSION_OPERATION_EXCEPTION, e);
        }
    }

    MessageConsumer createConsumer(Destination destination, MessageListener messageListener) {
        try {
            MessageConsumer consumer = session.createConsumer(destination);
            consumer.setMessageListener(messageListener);
            return consumer;
        } catch (JMSException e) {
            throw exceptionOf(SESSION_OPERATION_EXCEPTION, e);
        }
    }

    TextMessage createTextMessage(String payload) {
        try {
            TextMessage message = session.createTextMessage();
            message.setText(payload);
            return message;
        } catch (JMSException e) {
            throw exceptionOf(SESSION_OPERATION_EXCEPTION, e);
        }
    }

    void close() {
        try {
            session.close();
        } catch (JMSException e) {
            throw exceptionOf(SESSION_OPERATION_EXCEPTION, e);
        }
    }

}
