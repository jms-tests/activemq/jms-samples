package org.gol.broker;

import javax.jms.Destination;

public class BrokerQueueProducer extends BrokerProducer {


    private BrokerQueueProducer(BrokerConnection brokerConnection, String destinationName) {
        super(brokerConnection, destinationName);
    }

    public static BrokerQueueProducer of(BrokerConnection brokerConnection, String destinationName) {
        return new BrokerQueueProducer(brokerConnection, destinationName);
    }

    @Override
    Destination createDestination(String name) {
        return getBrokerSession().createQueue(name);
    }
}
