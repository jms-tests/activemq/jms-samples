package org.gol.broker;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;

import static org.gol.broker.BrokerException.Message.PRODUCER_OPERATION_EXCEPTION;
import static org.gol.broker.BrokerException.exceptionOf;

public abstract class BrokerConsumer implements AutoCloseable {

    private final BrokerSession session;
    private final MessageConsumer messageConsumer;

    BrokerConsumer(BrokerConnection brokerConnection, String destinationName, MessageListener messageListener) {
        this.session = BrokerSession.from(brokerConnection);
        Destination destination = createDestination(destinationName);
        this.messageConsumer = session.createConsumer(destination, messageListener);
    }

    abstract Destination createDestination(String name);

    @Override
    public void close() {
        try {
            messageConsumer.close();
            session.close();
        } catch (JMSException e) {
            throw exceptionOf(PRODUCER_OPERATION_EXCEPTION, e);
        }
    }

    BrokerSession getBrokerSession() {
        return session;
    }
}
