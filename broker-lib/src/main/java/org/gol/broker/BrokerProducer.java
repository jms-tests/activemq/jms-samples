package org.gol.broker;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;

import static org.gol.broker.BrokerException.Message.CANNOT_CREATE_PRODUCER;
import static org.gol.broker.BrokerException.Message.PRODUCER_OPERATION_EXCEPTION;
import static org.gol.broker.BrokerException.exceptionOf;

public abstract class BrokerProducer implements AutoCloseable {

    private final BrokerSession session;
    private final MessageProducer messageProducer;

    BrokerProducer(BrokerConnection brokerConnection, String destinationName) {
        this.session = BrokerSession.from(brokerConnection);
        Destination destination = createDestination(destinationName);
        this.messageProducer = session.createProducer(destination);
    }

    abstract Destination createDestination(String name);

    public void sendTextMessage(String payload) {
        try {
            TextMessage txtMessage = session.createTextMessage(payload);
            messageProducer.send(txtMessage);
        } catch (JMSException e) {
            throw exceptionOf(CANNOT_CREATE_PRODUCER, e);
        }
    }

    @Override
    public void close() {
        try {
            messageProducer.close();
            session.close();
        } catch (JMSException e) {
            throw exceptionOf(PRODUCER_OPERATION_EXCEPTION, e);
        }
    }

    BrokerSession getBrokerSession() {
        return session;
    }
}
