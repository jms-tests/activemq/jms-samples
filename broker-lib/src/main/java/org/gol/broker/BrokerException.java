package org.gol.broker;

public class BrokerException extends RuntimeException {

    enum Message {
        CANNOT_CREATE_CONNECTION("Cannon create connection"),
        CONNECTION_OPERATION_EXCEPTION("Problem occurred with broker connection"),
        CANNOT_CREATE_SESSION("Cannon create session"),
        SESSION_OPERATION_EXCEPTION("Problem occurred with broker session"),
        CANNOT_CREATE_PRODUCER("Cannon create producer"),
        PRODUCER_OPERATION_EXCEPTION("Problem occurred with broker producer"),
        SSL_KEY_OR_TRUST_STORE_ERROR("Cannot load key or trust store to SSL connection factory");

        private String description;

        Message(String description) {
            this.description = description;
        }
    }

    public static BrokerException exceptionOf(Message message, Throwable cause) {
        return new BrokerException(message.description, cause);
    }

    private BrokerException(String message, Throwable cause) {
        super(message, cause);
    }
}
