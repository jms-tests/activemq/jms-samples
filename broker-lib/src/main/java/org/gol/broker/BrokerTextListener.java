package org.gol.broker;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BrokerTextListener implements MessageListener {

    public BrokerTextListener() {
        super();
        log.debug("BrokerTextListener has been created");
    }

    @Override
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            try {
                String text = ((TextMessage) message).getText();
                log.info("message received: {}", text);
            } catch (JMSException e) {
                log.error("unable to read message payload", e);
            }
        } else {
            log.error("received unsupported message type");
        }
    }
}
