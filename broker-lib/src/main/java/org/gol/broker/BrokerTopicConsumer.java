package org.gol.broker;

import javax.jms.Destination;
import javax.jms.MessageListener;

public class BrokerTopicConsumer extends BrokerConsumer {


    private BrokerTopicConsumer(BrokerConnection brokerConnection, String destinationName, MessageListener messageListener) {
        super(brokerConnection, destinationName, messageListener);
    }

    public static BrokerTopicConsumer of(BrokerConnection brokerConnection, String destinationName, MessageListener messageListener) {
        return new BrokerTopicConsumer(brokerConnection, destinationName, messageListener);
    }

    @Override
    Destination createDestination(String name) {
        return getBrokerSession().createTopic(name);
    }
}
