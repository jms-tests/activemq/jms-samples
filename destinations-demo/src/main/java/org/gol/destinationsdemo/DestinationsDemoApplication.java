package org.gol.destinationsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DestinationsDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DestinationsDemoApplication.class, args);
    }

}
