package org.gol.destinationsdemo.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.SimpleMessageConverter;

import javax.jms.ConnectionFactory;

import static org.springframework.jms.support.converter.MessageType.TEXT;

@Configuration
public class JmsConfig {

    public static final String QUEUE_FACTORY = "queueListenerFactory";
    public static final String TOPIC_FACTORY = "topicListenerFactory";
    public static final String DURABLE_TOPIC_FACTORY_A = "durableTopicListenerFactoryA";
    public static final String DURABLE_TOPIC_FACTORY_B = "durableTopicListenerFactoryB";

    @Value("${spring.activemq.broker-url}")
    private String brokerUrl;

    @Value("${spring.activemq.user}")
    private String userName;

    @Value("${spring.activemq.password")
    private String userPassword;

    /**
     * Serialize message content to json using TextMessage
     *
     * @return {@link MessageConverter object}
     */
    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(TEXT);
        return converter;
    }

    @Bean
    public MessageConverter simpleMessageConverter() {
        return new SimpleMessageConverter();
    }

    @Bean
    public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory, MessageConverter jacksonJmsMessageConverter) {
        JmsTemplate jmsTemplate = new JmsTemplate();
        jmsTemplate.setConnectionFactory(connectionFactory);
        jmsTemplate.setMessageConverter(jacksonJmsMessageConverter);
        return jmsTemplate;
    }

    @Bean(QUEUE_FACTORY)
    public JmsListenerContainerFactory queueListenerFactory(
            ConnectionFactory connectionFactory,
            DefaultJmsListenerContainerFactoryConfigurer configurer,
            MessageConverter simpleMessageConverter) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        factory.setMessageConverter(simpleMessageConverter);
        factory.setPubSubDomain(false);
        return factory;
    }

    @Bean(TOPIC_FACTORY)
    public JmsListenerContainerFactory topicListenerFactory(
            ConnectionFactory connectionFactory,
            DefaultJmsListenerContainerFactoryConfigurer configurer,
            MessageConverter simpleMessageConverter) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        factory.setMessageConverter(simpleMessageConverter);
        factory.setPubSubDomain(true);
        return factory;
    }

    @Bean(DURABLE_TOPIC_FACTORY_A)
    public JmsListenerContainerFactory durableTopicListenerFactoryA(
            DefaultJmsListenerContainerFactoryConfigurer configurer,
            MessageConverter simpleMessageConverter) {
        return createDurableTopicListenerFactory("durable-subscriber-A", configurer, simpleMessageConverter);
    }

    @Bean(DURABLE_TOPIC_FACTORY_B)
    public JmsListenerContainerFactory durableTopicListenerFactoryB(
            DefaultJmsListenerContainerFactoryConfigurer configurer,
            MessageConverter simpleMessageConverter) {
        return createDurableTopicListenerFactory("durable-subscriber-B", configurer, simpleMessageConverter);
    }

    private JmsListenerContainerFactory createDurableTopicListenerFactory(
            String clientId,
            DefaultJmsListenerContainerFactoryConfigurer configurer,
            MessageConverter messageConverter) {
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(userName, userPassword, brokerUrl);
        connectionFactory.setClientID(clientId);

        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        factory.setMessageConverter(messageConverter);
        factory.setPubSubDomain(true);
        factory.setSubscriptionDurable(true);
        return factory;
    }

}
