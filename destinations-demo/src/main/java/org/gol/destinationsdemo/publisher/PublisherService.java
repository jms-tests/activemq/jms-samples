package org.gol.destinationsdemo.publisher;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PublisherService {

    private static final String TEST_QUEUE = "test-queue";
    private static final String TEST_TOPIC = "test-topic";
    private static final String TEST_VIRTUAL_TOPIC = "VirtualTopic.test-topic";

    private final @NonNull JmsTemplate jmsTemplate;

    public <T> void sendToQueue(T message) {
        jmsTemplate.convertAndSend(new ActiveMQQueue(TEST_QUEUE), message);
    }

    public <T> void sendToTopic(T message) {
        jmsTemplate.convertAndSend(new ActiveMQTopic(TEST_TOPIC), message);
    }

    public <T> void sendToVirtualTopic(T message) {
        jmsTemplate.convertAndSend(new ActiveMQTopic(TEST_VIRTUAL_TOPIC), message);
    }
}
