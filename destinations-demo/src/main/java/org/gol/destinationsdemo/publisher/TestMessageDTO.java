package org.gol.destinationsdemo.publisher;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TestMessageDTO {

    private Integer number;
    private String text;

    public static TestMessageDTO of(Integer number, String text) {
        TestMessageDTO message = new TestMessageDTO();
        message.number = number;
        message.text = text;
        return message;
    }

}
