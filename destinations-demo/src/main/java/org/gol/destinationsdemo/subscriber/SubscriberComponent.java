package org.gol.destinationsdemo.subscriber;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

import static org.gol.destinationsdemo.config.JmsConfig.DURABLE_TOPIC_FACTORY_A;
import static org.gol.destinationsdemo.config.JmsConfig.DURABLE_TOPIC_FACTORY_B;
import static org.gol.destinationsdemo.config.JmsConfig.QUEUE_FACTORY;
import static org.gol.destinationsdemo.config.JmsConfig.TOPIC_FACTORY;

@Slf4j
@Component
public class SubscriberComponent {

    private static final String TEST_QUEUE = "test-queue";
    private static final String TEST_TOPIC = "test-topic";

    private static final String TEST_VIRTUAL_TOPIC_CONSUMER_A = "Consumer.A.VirtualTopic.test-topic";
    private static final String TEST_VIRTUAL_TOPIC_CONSUMER_B = "Consumer.B.VirtualTopic.test-topic";

    @JmsListener(destination = TEST_QUEUE, concurrency = "1", containerFactory = QUEUE_FACTORY)
    public void handleQueueA(Message<String> message) {
        log("handleQueueA", message);
        timeout();
    }

    @JmsListener(destination = TEST_QUEUE, concurrency = "3-5", containerFactory = QUEUE_FACTORY)
    public void handleQueueB(Message<String> message) {
        log("handleQueueB", message);
        timeout();
    }

    @JmsListener(destination = TEST_TOPIC, containerFactory = TOPIC_FACTORY)
    public void handleTopicA(Message<String> message) {
        log("handleTopicA", message);
        timeout();
    }

    @JmsListener(destination = TEST_TOPIC, containerFactory = TOPIC_FACTORY)
    public void handleTopicB(Message<String> message) {
        log("handleTopicB", message);
        timeout();
    }

    @JmsListener(destination = TEST_TOPIC, subscription = "Durable-A", containerFactory = DURABLE_TOPIC_FACTORY_A)
    public void handleDurableTopicA(Message<String> message) {
        log("handleDurableTopicA", message);
        timeout();
    }

    @JmsListener(destination = TEST_TOPIC, subscription = "Durable-B", containerFactory = DURABLE_TOPIC_FACTORY_B)
    public void handleDurableTopicB(Message<String> message) {
        log("handleDurableTopicB", message);
        timeout();
    }

    @JmsListener(destination = TEST_VIRTUAL_TOPIC_CONSUMER_A, containerFactory = QUEUE_FACTORY)
    public void handleVirtualTopicA(Message<String> message) {
        log("handleVirtualTopicA", message);
        timeout();
    }

    @JmsListener(destination = TEST_VIRTUAL_TOPIC_CONSUMER_B, concurrency = "3-5", containerFactory = QUEUE_FACTORY)
    public void handleVirtualTopicB(Message<String> message) {
        log("handleVirtualTopicB", message);
        timeout();
    }

    private void log(String name, Message message) {
        log.info(String.format("%s - %s - %s", name, Thread.currentThread().getName(), message.getPayload()));
    }

    private void timeout() {
        try {
            Thread.sleep(RandomUtils.nextLong(100, 500));
        } catch (InterruptedException e) {
            log.warn("Interrupted", e);
            Thread.currentThread().interrupt();
        }
    }

}
