package org.gol.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import static java.math.BigDecimal.valueOf;

public class ExecutionTimeMeter {
    private String title;
    private long startTime;
    private long endTime;

    private ExecutionTimeMeter(String title) {
        this.title = title;
        this.startTime = System.nanoTime();
    }

    public static ExecutionTimeMeter start(String title) {
        return new ExecutionTimeMeter(title);
    }

    public ExecutionTimeMeter stop() {
        this.endTime = System.nanoTime();
        return this;
    }

    public long getDurationInNanos() {
        return endTime - startTime;
    }

    public long getDurationInMillis() {
        return valueOf(endTime - startTime)
                .scaleByPowerOfTen(-6)
                .setScale(0, RoundingMode.HALF_UP)
                .longValue();
    }

    public double getDurationInSeconds() {
        return valueOf(endTime - startTime)
                .scaleByPowerOfTen(-9)
                .setScale(3, RoundingMode.HALF_UP)
                .doubleValue();
    }

    public String getFormattedOutput() {
        double seconds = getDurationInSeconds();
        return ("Execution time meter - "
                + this.title + " - total time: "
                + new DecimalFormat("#.###").format(seconds) + " s");
    }
}
