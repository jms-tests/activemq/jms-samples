package org.gol.util;

import org.junit.jupiter.api.Test;

import static java.lang.Math.round;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ExecutionTimeMeterTest {

    @Test
    void meterOutputTest() throws InterruptedException {
        ExecutionTimeMeter meter = ExecutionTimeMeter.start("test");
        Thread.sleep(1234);
        meter.stop();

        assertEquals(meter.getDurationInMillis(), round((double) meter.getDurationInNanos() / 1000000));
        assertEquals(meter.getDurationInSeconds() * 1000, meter.getDurationInMillis());
        assertThat(meter.getFormattedOutput(), containsString(Double.toString(meter.getDurationInSeconds())));
    }
}