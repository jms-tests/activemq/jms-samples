#!/bin/bash

echo '## Create a keystore for the broker SERVER'
keytool -genkey -alias amq-server -keyalg RSA -keysize 2048 -validity 3650 -keystore amq-server.ks

echo
echo '## Export the broker SERVER certificate from the keystore'
keytool -export -alias amq-server -keystore amq-server.ks -file amq-server_cert

echo
echo '## Create the CLIENT keystore'
keytool -genkey -alias amq-client -keyalg RSA -keysize 2048 -validity 3650 -keystore amq-client.ks

echo
echo '## Import the previous exported broker certificate into a CLIENT truststore'
keytool -import -alias amq-server -keystore amq-client.ts -file amq-server_cert

echo
echo '## Export the client certificate from the keystore'
keytool -export -alias amq-client -keystore amq-client.ks -file amq-client_cert

echo
echo '## Import the client exported certificate into a broker SERVER truststore'
keytool -import -alias amq-client -keystore amq-server.ts -file amq-client_cert
